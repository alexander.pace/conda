#!/bin/bash
#
# IGWN Conda Distribution tests for LSCSoft GLUE
#

# run everything in a temporary directory
TMPDIR=$(mktemp -d -t lscsoft-glue-XXXXXXXXXX)
trap "cd ~ && rm -rf ${TMPDIR}" EXIT
pushd ${TMPDIR}

# download the original tarball which includes the tests/
VERSION=$(python -c "import glue; print(glue.__version__)")
URL="https://pypi.io/packages/source/l/lscsoft-glue/lscsoft-glue-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/test" || {
	echo "download failed, skipping...";
	exit 77;
}

# run the test suite
make -C test VERBOSE=1 V=1
