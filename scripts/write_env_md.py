#!/usr/bin/env python

"""Format the current list of environments for inclusion in the docs
"""

import json
import platform
import subprocess
import sys
import tempfile
from operator import itemgetter
from pathlib import Path

import yaml

from tabulate import tabulate

ANACONDA_CHANNELS = {
    "pkgs/free",
    "pkgs/main",
}

BASEDIR = Path(__file__).resolve().parent.parent
SUBDIR = "linux" if platform.system().lower() == "linux" else "osx"


def find_environments(path=None):
    if path is None:
        path = BASEDIR / "environments" / SUBDIR
    return sorted(Path(path).rglob("*.yaml"))


def write_environment(path, file=None):
    path = Path(path)
    with path.open("rb") as fobj:
        env = yaml.load(fobj, Loader=yaml.SafeLoader)

    # render environment
    prefix = tempfile.mktemp(prefix=f"render-{env['name']}")
    cmd = [
        "conda",
        "create",
        "--json",
        "--dry-run",
        "--prefix", prefix,
    ] + env["dependencies"]
    rendered = json.loads(subprocess.check_output(cmd))

    # write name
    print(f"# {env['name']}", file=file)

    # write link
    print(
        f"**Download:** "
        f"[linux-64](linux/{path.name}), "
        f"[osx-64](osx/{path.name})  ",
        f"[win-64](win/{path.name})  ",
        file=file,
    )
    channels = env['channels']
    if 'nodefaults' not in channels:
        channels.insert(0, 'defaults')
    print(
        "**Channels:** `{0}`  ".format("`, `".join(channels)),
        file=file,
    )
    print(
        f"""**Activate:**

```
conda activate {env['name']}
```""",
        file=file,
    )
    print(
        f"""**Install:**

```
conda env create --file {path.name}
```""",
        file=file,
    )

    # write packages
    print(
        f"""**Packages:**

!!! note "Package list for {SUBDIR}"
    The following package table describes the environment contents
    on **{SUBDIR}**, see the above **Download** links for environment
    YAML files for all supported platforms.
""",
        file=file,
    )
    rows = []
    for pkg in sorted(rendered["actions"]["LINK"], key=itemgetter("name")):
        cloudchannel = ('anaconda' if pkg['channel'] in ANACONDA_CHANNELS else
                        pkg['channel'])
        cloudurl = f"https://anaconda.org/{cloudchannel}/{pkg['name']}/"
        rows.append((
            f"[{pkg['name']}]({cloudurl})",
            pkg['version'],
            f"`{pkg['build_string']}`",
            pkg['channel'],
        ))
    table = tabulate(
        rows,
        headers=('Name', 'Version', 'Build', 'Channel'),
        tablefmt="github",
    )
    print(table, file=file)


def main(verbose=False):
    # copy all environment files to output
    for env in find_environments():
        envfn = Path("docs") / "environments" / (env.stem + '.md')
        if verbose:
            rel = env.relative_to(Path.cwd())
            print(f"Working on {rel!s} -> {envfn!s}...", end=" ")
        with open(envfn, 'w') as envf:
            write_environment(env, file=envf)
        if verbose:
            print("done")


if __name__ == "__main__":
    sys.exit(main(verbose=True))
