#!/bin/sh
#
# These tests rely on the fact that the parent environment has a valid
# kerberos keytab

set -e

# check tickets
klist -s || {
	echo "no valid kerberos ticket to use" 1>&2;
	exit 77;
}

# use temporary directory
_TMPDIR=$(mktemp -d)
trap "cd - && rm -rf ${_TMPDIR}" EXIT
cd ${_TMPDIR}

# get random filename
CERT_FILE="${_TMPDIR}/x509"
COOKIE_FILE="${_TMPDIR}/cookies"

# generate an X.509 certificate
ecp-get-cert \
	--kerberos \
	--identity-provider login.ligo.org \
	--file ${CERT_FILE} \
	--debug \
	--verbose \
 || {
    echo "ecp-get-cert --kerberos failed, skipping..." 1>&2;
    exit 77;
}

# print the information
ecp-cert-info --file ${CERT_FILE}

# get a cookie from CIT
ecp-get-cookie \
	--kerberos \
	--identity-provider login.ligo.org \
	--cookiefile ${COOKIE_FILE} \
	--debug \
	--verbose \
	https://ldas-jobs.ligo.caltech.edu/ \
 || {
    echo "cookie generation failed, skipping" 1>&2;
    exit 77;
}

# use the cookie to curl a URL
test $(
ecp-curl \
	--identity-provider login.ligo.org \
	--cookiefile ${COOKIE_FILE} \
	https://ldas-jobs.ligo.caltech.edu/~duncan.macleod/hello.html
) == "HELLO"

# remove the credential
ecp-get-cert --file ${CERT_FILE} --destroy --verbose
