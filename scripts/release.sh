#!/bin/bash
#
# Create a new stable release of the IGWN Conda Distribution,
# based on today's date
#

CANONICAL_REMOTE="computing/conda"

# echo a command to the shell as "$ <command>" and then run it
echo_and_execute() {
    echo "$ $@"
    "$@"
}

# note: get date from internet in case system clock is off
tagdate=$(date -d $(curl -s time.nist.gov:13 --http0.9 | cut -d\  -f2) +%Y%m%d)
datestr=$(date -d ${tagdate} +"%d %B %Y")
tagnum=$(git tag --list ${tagdate}.* | wc -l)
if [ ${tagnum} -eq 0 ]; then
    tag="${tagdate}"
else
    tag="${tagdate}.${tagnum}"
fi
echo "Tagging ${tag}"

echo "-- Checking out (up-to-date) master branch"
for remote in $(git remote); do
    git remote get-url ${remote} | grep -q "${CANONICAL_REMOTE}" && break || continue
done
echo "Using remote '${remote}'"
echo_and_execute git fetch ${remote}
echo_and_execute git checkout master
echo_and_execute git pull
echo "-- Tagging"
echo_and_execute git tag --sign ${tag} -m "IGWN Conda Distribution release for ${datestr}"
echo "-- Pushing"
echo_and_execute git push ${remote} ${tag} --signed=if-asked --verbose --verify

cat <<EOF
-- Now send this email to ldg-announce@ligo.org:

-------------------------------------------------
Dear all,

The IGWN Conda Distribution has been updated with a new environment set dated ${tag}.
The stable 'igwn-pyXY' environments in CVMFS will be updated to link to the igwn-pyXY-${tag} environments; these changes should be visible in CVMFS in a few hours.

For a summary of the changes for each stable environment, please see the release notes here:

https://git.ligo.org/computing/conda/-/releases/${tag}

For a full diff between old and new environments please refer to 'conda list'.

If you have any questions, please post a ticket on the Computing Helpdesk

https://git.ligo.org/computing/helpdesk/-/issues/

or for documentation of the IGWN Conda Distribution itself, including full listings of the current stable, proposed, and testing environments, please see

https://computing.docs.ligo.org/conda/


Thanks
The IGWN Conda Distribution team
-------------------------------------------------
EOF
