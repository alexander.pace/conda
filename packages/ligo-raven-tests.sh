#!/bin/bash
#
# IGWN Conda Distribution tests for ligo-raven
#

# test scripts
raven_coinc_search_gracedb --help
raven_fetch_external_triggers --help
raven_file_cache_monitor --help
raven_gcn_web_scraper --help
