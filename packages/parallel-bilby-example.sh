#!/bin/bash

set -e

if [[ "$(whoami)" == "root" ]] && [[ -z ${CI_JOB_IMAGE} ]]; then
	echo "Cannot execute mpi test as root outside of docker, skipping..." 2>&1
	exit 77
fi


VERSION=$(
conda list parallel-bilby --json | \
python -c "import json, sys; print(json.load(sys.stdin)[0]['version'])"
)

# run everything in a temporary directory
TMPDIR=$(mktemp -d -t parallel_bilby-XXXXXXXXXX)
trap "cd ~ && rm -rf ${TMPDIR}" EXIT
pushd ${TMPDIR}

# download the example files for the relevant version
URL="https://git.ligo.org/lscsoft/parallel_bilby/-/archive/${VERSION}/parallel_bilby-${VERSION}.tar.gz"
EXAMPLE_DIR="examples/GW150914_IMRPhenomPv2"
echo "Extracting ${EXAMPLE_DIR} from ${URL}..."
curl -Ls ${URL} | tar -xzf - --strip-components=3 $(test $(uname) = "Linux" && echo "--wildcards") "*/${EXAMPLE_DIR}" || {
	echo "download failed, skipping...";
	exit 77;
}

# modify config file to make it faster
sed -n -E \
	`# disable distance marginalization` \
	-e '/distance-marginalization=/c\distance-marginalization = False' \
	`# disable phase marginalization` \
	-e '/phase-marginalization=/c\phase-marginalization = False' \
	`# restrict mpi to a single core` \
	-e '/nodes( )?=/cnodes = 1' \
	-e '/n-parallel( )?=/cn-parallel = 1' \
	-e '/ntasks-per-node( )?=/cntasks-per-node = 1' \
	`# set nlive` \
	-e '/nlive( )?=/cnlive = 5000' \
	`# set dlogz (add it if it doesn't exist)` \
	-e '/^dlogz( )?=( )?/!p;$adlogz = 210' \
	`# set no_plot (add it if it doesn't exist)` \
	-e '/^no_plot( )?=( )?/!p;$ano_plot = True' \
	GW150914.ini \
`# remove duplicate lines (preserving order)` \
| grep -v "^#" | cat -n | sort -k2 | uniq -f1 | sort -n -k1 | cut -f2- > GW150914.igwn-conda.ini

echo "================================"
cat GW150914.igwn-conda.ini
echo "================================"

# generate workflow
parallel_bilby_generation GW150914.igwn-conda.ini

# run the script
if [[ "$(whoami)" == "root" ]]; then  # tell mpirun we know what we're doing
	export OMPI_ALLOW_RUN_AS_ROOT=1
	export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
fi
bash -e $(test -o xtrace && echo "-x") outdir/submit/analysis_GW150914_0.sh
