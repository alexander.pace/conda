# Package change workflow

This page attempts to describe the change workflow in order to implement
change requests that have been detailed on an SCCB ticket.

## Determining version numbers and build strings

In order to add a new package, or upgrade an existing package, the new package
version and build string need to be determined.

The version number should be declared in the SCCB ticket, which can then be used
to determine the build string by using the
[`search.py` script](https://git.ligo.org/computing/conda/-/blob/testing/scripts/search.py)
(from _this_ repo):

```console
python ./scripts/search.py <package> <version>
```

!!! example "search.py output"
    ```console
    > python ./scripts/search.py python-framel 8.40.1
    ```
    This will print something like this:

    ```yaml
    package:
      name: python-framel
      version: 8.40.1
      build_string: py36h785e9b2_0  # [linux and x86_64 and py==36]
      build_string: py37h03ebfcd_0  # [linux and x86_64 and py==37]
      build_string: py38h8790de6_0  # [linux and x86_64 and py==38]
      build_string: py36h255dfe6_0  # [osx and x86_64 and py==36]
      build_string: py37h10e2902_0  # [osx and x86_64 and py==37]
      build_string: py38h65ad66c_0  # [osx and x86_64 and py==38]
      build_string: py36h9902d54_0  # [win and x86_64 and py==36]
      build_string: py37h9dff50a_0  # [win and x86_64 and py==37]
      build_string: py38h377fac3_0  # [win and x86_64 and py==38]
    ```

!!! tip "Restricting the build"
    You can use the `--build` option to specify a build string (or wildcard)
    to restrict the results to a single build.

Once you have the new YAML content you can either add a new file for the new
package, or modify an existing file to upgrade an existing package.

## Getting packages into testing

To get packages in testing,
[open a merge request](https://git.ligo.org/computing/conda/merge_requests/new?merge_request%5Btarget_branch%5D=testing) against `testing`.
The CI will run to validate things.

Once the CI passes, **the MR can be merged immediately** in order to deploy the
new packages into the testing environments.

## Migrating packages from testing to proposed

To migrate packages from `testing` into `proposed`:

1. go back to the merge request you posted for `testing`,
   and click the _Cherry-pick_ button,
2. in the popup, under _Pick into branch_, select `master`
3. ensure that the _Start a new merge request with these changes_ radio box
   is **checked**
4. Click _Cherry-pick_.

!!! tip "Apply the right labels"
    Please then apply the _sccb::pending_ label to indicate that the SCCB
    is still considering this request.
    Please also apply any other labels that seem appropriate.

!!! warning "Wait for SCCB approval"
    Once the CI passes, **wait until the SCCB approves the request**.

As soon as the SCCB approves the request, the MR should then be merged to
deploy the new packages into the proposed environments.

## Creating a new stable release

When deployment time arrives, a new stable distribution should be created,
see [_Tagging a release_](./release.md).

## Troubleshooting

### Determining why a package isn't available {: #debug-missing }

If `search.py` errors, it may be because a package (at a particular version)
isn't available in `conda-forge`.
You can perform a basic search to see if _any_ version of that package
is available (useful if the requested package is new):

```bash
conda search my-new-package
```

From here there are three likely outcomes:

1. The package isn't available at any version; it is likely that the requested
   package simply isn't packaged for conda yet.
   In that case you can check the conda-forge
   [`staged-recipes`](https://github.com/conda-forge/staged-recipes/)
   repository's
   [Pull Requests list](https://github.com/conda-forge/staged-recipes/pulls)
   to see if there's a PR marked _Add my-new-package_.

       If there _is_ a PR, just keep track of it and add the package when it
       becomes available.
       If there _isn't_ a PR, you should contact the developer to ask whether they
       plan to add one, if they don't have a plan, give them a stern look and
       ask them to make one.

2. The package isn't available at the requested version, only earlier ones;
   it is likely that the conda-forge feedstock hasn't yet been updated to
   reflect the new upstream release.
   In this case you can check the feedstock repo Pull Requests list at

       https://github.com/conda-forge/my-new-package-feedstock/pulls/

       (replace `my-new-package` with an actual package name). If the feedstock
       is well formed the conda-forge autotick bot should make a PR automatically
       when an upstream tarball is uploaded (e.g. to pypi.python.org), otherwise
       you might have to open a ticket on the feedstock, or just directly contact
       the feedstock maintainers (likely the developers of the package themselves)
       to see what's going on.

3. The package isn't available at the specifically requested version, but is
   available at newer versions. In this case it's more trouble than it's worth
   to attempt to backport the given version for conda-forge, you should go back
   to the requester and see if the closest matching version that does exist in
   conda-forge will fulfil their request.
