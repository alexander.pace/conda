# Environments

The IGWN Conda Distribution includes a number of environments, covering
different versions of Python, and _testing_, _proposed_, and _stable_ package
lists.
You can use these environments either directly from the CVMFS distribution
(see [_Pre-built environments_](#pre-built-environments) below), or by
installing them manually after downloading the relevant YAML files
(you can find download links on the details page for each environment).

## Supported Python versions {: #python-versions }

The IGWN Conda Distribution roughly follows the
[NEP 29](https://numpy.org/neps/nep-0029-deprecation_policy.html)
schedule for supporting minor Python versions, with the additional
requirement that each Python version is fully supported by conda-forge
(the relevant migration has been completed and that version is part of the
[conda-forge pinned packages](https://github.com/conda-forge/conda-forge-pinning-feedstock)
).
This means that:

!!! example "IGWN Conda Distribution Python version support"

    IGWN Conda Distribution environments will be supported for all minor
    versions of (C)Python released within the 42 months prior to the
    Distribution release date that are also supported by conda-forge,
    and at minimum the two latest minor versions supported by conda-forge.

## Environment types {: #definitions }

!!! note "Naming conventions"
    All environments use the `igwn-pyXY` prefix, where `X.Y` is the version
    of Python included in the environment.

### Stable (`igwn-pyXY`) {: #stable }

The _stable_ environments represent the latest set of packages and versions
that have been approved for use by the IGWN
[Software Change Control Board](https://sccb.docs.ligo.org/).
These are recommended for use in all production analyses.

!!! tip "`igwn-pyXY` is a symlink"
    The latest stable environments (`igwn-pyXY`) are symbolic links
    to the latest tagged stable environments (`igwn-pyXY-YYYYMMDD`),
    see below for more details.

### Old stable (`igwn-pyXY-YYYYMMDD`) {: #old-stable }

Past _stable_ environments are preserved using the date tag for when they
were created, using the `YYYYMMDD.MICRO` [CalVer](https://calver.org)
versioning scheme.

Old stable environments will preserved until it is known that no current,
or under-review analyses, require them.

### Proposed

The `igwn-pyXY-proposed` environments represent the latest set of packages
that have been approved for inclusion in a future stable environment.

!!! warning "Proposed environments are subject to continuous change"
    The proposed environments can and will change from day-to-day as
    more packages are approved by the SCCB, so no production analyses
    should rely upon them.

### Testing

The `igwn-pyXY-testing` environments include the latest packages
that have been requested for inclusion in a future stable environment via
an [SCCB request](https://sccb.docs.ligo.org/requests/).

!!! warning "Testing environments are subject to continuous change"
    The testing environments can and will change from day-to-day as
    more packages are approved by the SCCB, so no production analyses
    should rely upon them.

## Available environments {: #environments }

The following environments are always available:

- [`igwn-py37`](igwn-py37.md)
- [`igwn-py37-proposed`](igwn-py37-proposed.md)
- [`igwn-py37-testing`](igwn-py37-testing.md)
- [`igwn-py38`](igwn-py38.md)
- [`igwn-py38-proposed`](igwn-py38-proposed.md)
- [`igwn-py38-testing`](igwn-py38-testing.md)
- [`igwn-py39`](igwn-py39.md)
- [`igwn-py39-proposed`](igwn-py39-proposed.md)
- [`igwn-py39-testing`](igwn-py39-testing.md)

Additionally, tagged stable environments will be present, use the `conda`
command line tools to determine what environments are available and what
packages they contain.

## Making changes {: #change-control }

See [_Change control_](../change-control.md) for details on how the package
lists are managed, and how to request or propose changes.
