#!/usr/bin/env python3

"""Render the IGWN Conda environments from the package lists
"""

import argparse
import atexit
import json
import logging
import os
import shutil
import subprocess
import sys
import tempfile
from pathlib import Path

import git

import jinja2

import yaml

import utils

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

# create a logger
LOGGER = utils.create_logger("render")

# git repo
REPO = git.Repo()
try:
    ACTIVE_BRANCH = REPO.active_branch.name
except TypeError:
    if "CI_COMMIT_SHA" not in os.environ:
        raise
    ACTIVE_BRANCH = os.environ["CI_COMMIT_SHA"]

REMOTE_BRANCHES = [
    x.name[len(x.remote_name)+1:] for x in REPO.remotes.origin.refs if
    x.is_valid() and not x.name.endswith('HEAD')
]

# default file paths
_BASE_PATH = Path(__file__).parent.parent.resolve()
_DEFAULT_PACKAGES_PATH = _BASE_PATH / "packages"
_CONFIG_YAML_NAME = "igwn_conda_config.yaml"
_CONFIG_YAML_PATH = _BASE_PATH / _CONFIG_YAML_NAME
_UPSTREAM_YAML_NAME = "upstream.yaml"
_UPSTREAM_YAML_PATH = _DEFAULT_PACKAGES_PATH / _UPSTREAM_YAML_NAME
try:
    DEFAULT_CONFIG_YAML = _CONFIG_YAML_PATH.resolve(strict=True)
except FileNotFoundError:
    DEFAULT_CONFIG_YAML = None
try:
    DEFAULT_UPSTREAM_YAML = _UPSTREAM_YAML_PATH.resolve(strict=True)
except FileNotFoundError:
    DEFAULT_UPSTREAM_YAML = None

# YAML files to ignore
IGNORE = {
    "template.yaml",
    _UPSTREAM_YAML_NAME,
}

# are we on windows?
WINDOWS = sys.platform == "win32"

# supported test script formats and their executables
TEST_SCRIPT_FORMATS = {
    ".pl": "perl",
    ".py": "python",
    ".sh": "bash.exe -el" if WINDOWS else "bash -e",
}

# test file templates
TEST_TEMPLATE = {
    "imports": """
#!/usr/bin/env python{{ python_version }}
# -*- coding: utf-8 -*-

import importlib

import pytest

imports = [
{% for item in imports %}
    "{{ item }}",{% endfor %}
]


@pytest.mark.parametrize("module", imports)
def test_import(module):
    importlib.import_module(module)
""".strip(),
    "commands": """
#!/usr/bin/env python{{ python_version }}
# -*- coding: utf-8 -*-

import os
import subprocess

import pytest

commands = [
{%- for item in commands %}
    pytest.param(
        \"\"\"{{ item['cmd']|replace('"', '\\\\"') }}\"\"\",
        {%- if 'marks' in item %}
        marks=[{{ item['marks']|join(', ') }}],
        {%- endif %}
        {%- if 'id' in item %}
        id=\"\"\"{{ item['id']|replace('"', '\\\\"') }}\"\"\",
        {%- endif %}
    ),
{%- endfor %}
]


@pytest.mark.parametrize("command", commands)
def test_command(command):
    proc = subprocess.run(
        command,
        check=False,
        shell=True,
        env=os.environ,
    )
    if proc.returncode == 77:  # SKIP
        pytest.skip(f"{command!r} skipped")
    proc.check_returncode()
""".strip(),
}


def _latest_tag(default_branch="master"):
    """Returns the name of the most recent tag
    """
    try:
        return REPO.git.describe(abbrev=0)
    except git.GitCommandError:
        try:
            return os.environ["CI_COMMIT_TAG"]
        except KeyError:
            if default_branch in REMOTE_BRANCHES:
                return default_branch
            return ACTIVE_BRANCH


def _target_branch_ref(target):
    # target should be either 'master' (proposed) or 'testing'
    if os.getenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME") == target:
        # if this is a MR against the target, just use the current ref
        return os.getenv("CI_COMMIT_SHA")

    if target in REMOTE_BRANCHES:
        return target

    raise RuntimeError(
        "could not locate ref for {}, available branches: {}".format(
            target, ", ".join(map(str, REMOTE_BRANCHES)),
        ),
    )


def _git_checkout(ref):
    """Checkout a specific git reference
    """
    LOGGER.debug(f"$ git checkout {ref}")
    REPO.git.checkout(ref)
    try:
        active = REPO.head.reference.name
    except TypeError:
        try:
            active = REPO.git.describe()
        except git.GitCommandError:
            active = str(REPO.head.commit)
    LOGGER.debug(f"successfully checked out {active}")


def find_packages(*bases):
    """Find the YAML files for all packages
    """
    for base in bases:
        for pkgfile in base.rglob("*.yaml"):
            if pkgfile.name in IGNORE:
                continue
            yield pkgfile


def find_test_scripts(*bases):
    """Find test scripts inside the package dirs
    """
    for base in bases:
        for ext in TEST_SCRIPT_FORMATS:
            for script in base.rglob(f"*{ext}"):
                yield script


def define_environments(
        branch,
        config,
        upstream=None,
        python_version=None,
        unpin_upstream=False,
):
    # read upstream package list
    if upstream is not None:
        with open(upstream, "r") as fobj:
            upstream = fobj.read()

    environments = {}

    for pyver in config["python"]:
        pyv = ".".join(pyver.split(".")[:2])

        if python_version not in [None, pyv]:  # skip this version
            continue

        # construct environment name (e.g. 'igwn-py40-testing')
        py = int(pyv.replace(".", ""))
        envd = environments[pyv] = {}
        name = "{}py{}{}".format(
            config["prefix"],
            py,
            # add -{branch} suffix for everything except stable
            "" if branch == "stable" else "-{}".format(branch),
        )

        # build list of dependencies for this environment
        uplist = sorted(utils.parse_yaml(upstream, py=py)["dependencies"])
        if args.unpin_upstream:  # remove version pins if requested
            uplist = [x.split('=')[0] for x in uplist]
        deps = ["python={}".format(pyv)] + uplist

        # environment definition
        envd[branch] = {
            "name": name,
            "channels": config["channels"],
            "dependencies": deps,
            "tests": {"imports": [], "commands": []},
        }
    return environments


def parse_build_string(name, raw, **context):
    try:
        return jinja2.Template(raw).render(**context)
    except TypeError as exc:
        exc.args = (
            "failed to render build_string for {}: {}".format(
                name,
                str(exc),
            ),
        )
        raise


def _get_list(dict_, key):
    """Parse a YAML map option as a list, even if given as a string
    """
    val = dict_.get(key, []) or []
    if isinstance(val, str):
        return [x.strip() for x in val.split(",")]
    return val


def _parse_test_map(test, key="cmd"):
    """Parse a test command into a mappable

    Giving simple strings is fine, but structured information can
    be given to control how pytest executes the command.
    """
    if isinstance(test, str):
        return {key: test}

    # ensure that a the primary key was given
    assert isinstance(test.get(key), str), "invalid test declaration"

    # parse the marks to pass to pytest
    if "marks" in test:
        test["marks"] = [
            "pytest.mark." + mark.strip()
            for mark in _get_list(test, "marks")
        ]

    return test


def _parse_test_command(test):
    """Format a `test/command` entry into a pytest command dict
    """
    return _parse_test_map(test, key="cmd")


def _script_call_str(script):
    """Construct a shell call for a script based on its file extension
    """
    for suffix, stub in TEST_SCRIPT_FORMATS.items():
        if script.endswith(suffix):
            return f"{stub} {script}"
    raise ValueError(f"unrecognised script {script!r}")


def _parse_test_script(test):
    """Parse a `test/script` entry into a pytest command dict
    """
    test = _parse_test_map(test, key="name")
    test["id"] = test["name"]  # use the script name as the ID
    test["cmd"] = _script_call_str(test.pop("name"))
    return test


def _parse_pytest_test_command(test):
    """Parse a `test/pytest` entry into a pytest command dict
    """
    test = _parse_test_command(test)
    test["id"] = f"pytest {test['cmd']}"  # use what the user gave us as the ID
    test["cmd"] = _pytest_call_str(test["cmd"])
    return test


def _pytest_call_str(args):
    """Construct a pytest call
    """
    return " ".join((
        "python",
        "-m", "pytest",
        "--cache-clear",
        "--color", "yes",
        "--code-highlight", "yes",
        "--no-header",
        "-r a",
    )) + " " + args


TEST_PARSERS = {
    "pytest": _parse_pytest_test_command,
    "commands": _parse_test_command,
    "script": _parse_test_script,  # be forgiving and allow a singular
    "scripts": _parse_test_script,
}


def add_package(envdict, pkginfo, **context):
    # get metadata
    name = pkginfo["package"]["name"]
    version = str(pkginfo["package"]["version"])
    tests = pkginfo.get("test", {}) or {}
    build = parse_build_string(
        name,
        str(pkginfo["package"]["build_string"]),
        **context,
    )
    for branch in envdict:
        # add package spec to list
        envdict[branch]["dependencies"].append(
            "{}={}={}".format(
                name,
                version,
                build,
            ),
        )

        envtests = envdict[branch]["tests"]

        # -- parse tests

        # test/imports are simple
        envtests["imports"].extend(_get_list(tests, "imports"))

        # test/{commands,pytest,scripts} are all transformed into shell calls
        envdict[branch]["tests"]["commands"].extend(
            cmd
            for key, test_parser in TEST_PARSERS.items()
            for cmd in map(test_parser, _get_list(tests, key))
        )


def _should_retry_render(out):
    """Returns `True` if a failed ``conda create`` call should be retried
    """
    return (
        # conda
        out["exception_name"] in {"CondaHTTPError"}
        # mamba
        or (
            out["exception_name"] == "RuntimeError"
            and out["error"].startswith("RuntimeError(\"Download error")
        )
    )


def render_environment(
        env,
        retry=0,
        mamba=shutil.which("mamba") is not None,
):
    """Render the requested environment

    Parameters
    ----------
    env : `dict`
        the environment to render, should at least have a
        ``'dependencies'`` key that includes a list of packages
        to use

    retry : `int`, `bool`, optional
        whether to retry the rendering if it fails with an HTTP-style error

    mamba : `bool`, optional
        use ``mamba`` to attempt to render the environment instead of
        ``conda``; if the rendering passes, run the whole thing again using
        ``conda`` to return a canonical environment - this is useful if
        the rendering fails, since ``mamba`` will return a human-readable
        description of the conflicts in a fraction of the time
    """
    prefix = tempfile.mktemp(prefix="render-{}".format(env["name"]))
    cmd = [
        "mamba" if mamba else "conda",
        "create",
        "--json",
        "--yes",
        "--dry-run",
        "--prefix", prefix,
    ]
    deps = list(env["dependencies"])
    # print dependencies in quotes in case they use `>` or similar
    LOGGER.debug("$ {}".format(" ".join(cmd + list(map(repr, deps)))))
    proc = subprocess.run(
        cmd + deps,
        capture_output=True,
        check=False,
        text=True,
    )

    # handle errors
    try:
        proc.check_returncode()
    except subprocess.CalledProcessError as exc:
        try:
            out = json.loads(proc.stdout)
        except json.JSONDecodeError:
            error = proc.stdout
            retry = False
        else:
            error = out["error"]
        if retry and _should_retry_render(out):
            LOGGER.warning(out["error"].split("\n", 1)[0])
            LOGGER.debug("retrying...")
            return render_environment(env, retry=int(retry)-1, mamba=mamba)
        LOGGER.critical(error.strip())
        exc.cmd = " ".join(cmd)
        raise

    # if it passed using mamba, try the whole thing again using conda;
    # mamba resolves things different to conda, and Duncan trusts conda
    # more than mamba (as of 15/9/2021)
    # NOTE: this duplication adds a few minutes to resolution for solvable
    #       environments, but saves hours of work for unsolvable environments
    if mamba:
        LOGGER.debug("mamba run passed, trying again with conda...")
        return render_environment(env, retry=retry, mamba=False)

    # otherwise we have used conda and have a solved environment
    out = json.loads(proc.stdout)
    new = env.copy()
    new["dependencies"] = sorted([
        "{name}={version}={build_string}".format(**pkg) for
        pkg in out["actions"]["LINK"]
    ])
    return new


def render(name, ref, args):
    LOGGER.info("-- Rendering {} {}".format(name, "".ljust(30, "-")))

    # checkout git ref
    _git_checkout(ref)

    # parse configuration
    with open(args.config_file, "r") as fobj:
        config = utils.parse_yaml(fobj.read())
    LOGGER.info("Parsed configuration")

    environments = define_environments(
        name,
        config,
        upstream=args.upstream_file,
        python_version=args.python,
        unpin_upstream=args.unpin_upstream,
    )
    LOGGER.info("The following environments will be created:")
    for pyv in environments:
        for type_ in environments[pyv]:
            LOGGER.info(environments[pyv][type_]["name"])

    # -- read package list and render environments ---

    for pkgf in find_packages(*package_dirs):
        LOGGER.debug(f"parsing {pkgf.name}")
        with open(pkgf, "r") as fobj:
            pkgdef = fobj.read()
        for pyv in environments:
            py = int("".join(pyv.split(".", 2)))

            # parse YAML definitions for this version of python
            try:
                pkg = utils.parse_yaml(pkgdef, py=py)
            except Exception as exc:
                exc.args = (
                    "failed to parse {}: {}".format(pkgf, str(exc)),
                )
                raise

            # verify that we want this package for this version
            if pkg.get("skip", False):
                continue

            # double check that the same package isn't specified twice
            pkgname = pkg["package"]["name"]
            if any(x.startswith(pkgname) for x in environments[pyv][name]):
                raise ValueError("{!r} specified in {} and {}".format(
                    pkgname,
                    pkgf,
                    args.upstream_file,
                ))

            # parse info and store package for this environment set
            add_package(
                environments[pyv],
                pkg,
                py=py,
            )

    # -- render environments and write tests --------

    envbase = Path(args.output_dir).resolve()
    envbase.mkdir(exist_ok=True, parents=True)

    for pyv in environments:
        for envtype, env in environments[pyv].items():
            name = env["name"]
            tests = env.pop("tests")
            envdir = envbase / name
            envdir.mkdir(exist_ok=True)

            LOGGER.info("-- Finalising {}".format(name))

            env["dependencies"].sort()

            # render environment
            rendered = render_environment(env, retry=1)

            # write environment file
            yamlpath = envdir / "{}.yaml".format(name)
            with yamlpath.open("w") as out:
                yaml.dump(rendered, out, default_flow_style=False)
            LOGGER.info("Environment file written: {}".format(yamlpath))

            # write tests
            for test, template in TEST_TEMPLATE.items():
                testpath = envdir / "{}-test-{}.py".format(name, test)
                with testpath.open("w") as out:
                    print(
                        jinja2.Template(template).render(
                            python_version=pyv,
                            **{test: tests[test]}
                        ),
                        file=out,
                    )
                LOGGER.info(f"{test} tests script written: {testpath}")

            # copy test scripts into environment
            for script in find_test_scripts(*package_dirs):
                shutil.copyfile(script, envdir / script.name)
                LOGGER.debug(f"copied {script.name!r} to {envdir!s}")

# -- define command line and parse --------------

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "packages",
    metavar="DIR",
    nargs="*",
    default=[str(_BASE_PATH / "packages")],
    help="path to packages dir (can be given multiple times)",
)
parser.add_argument(
    "-m",
    "--config-file",
    metavar=_CONFIG_YAML_NAME,
    default=DEFAULT_CONFIG_YAML,
    required=DEFAULT_CONFIG_YAML is None,
    type=Path,
    help="path to %(metavar)s",
)
parser.add_argument(
    "-u",
    "--upstream-file",
    metavar=_UPSTREAM_YAML_NAME,
    default=DEFAULT_UPSTREAM_YAML,
    type=Path,
    help="path to upstream package list",
)
parser.add_argument(
    "-p",
    "--python",
    default=None,
    help="python version to render (default: %(default)s [all])",
)
parser.add_argument(
    "--stable-ref",
    default=_latest_tag(),
    help="name of stable ref",
)
parser.add_argument(
    "--proposed-ref",
    default=_target_branch_ref("master"),
    help="name of proposed branch",
)
parser.add_argument(
    "--testing-ref",
    default=_target_branch_ref("testing"),
    help="name of testing branch",
)
parser.add_argument(
    "--skip-stable",
    action="store_true",
    default=False,
    help="skip rendering stable environments",
)
parser.add_argument(
    "--skip-proposed",
    action="store_true",
    default=False,
    help="skip rendering proposed environments",
)
parser.add_argument(
    "--skip-testing",
    action="store_true",
    default=False,
    help="skip rendering testing environments",
)
parser.add_argument(
    "-o",
    "--output-dir",
    metavar="ENVDIR",
    default=_BASE_PATH / "environments",
    help="output directory for environments",
)
parser.add_argument(
    "-U",
    "--unpin-upstream",
    action="store_true",
    default=False,
    help="remove version pins from upstream",
)
parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    default=False,
    help="print verbose output",
)
args = parser.parse_args()

if args.verbose:
    LOGGER.setLevel(logging.DEBUG)

package_dirs = set(Path(d).resolve() for d in args.packages)

# -- process each ref ---------------------------

atexit.register(_git_checkout, ACTIVE_BRANCH)

for name, (ref, skip) in {
        "stable": (args.stable_ref, args.skip_stable),
        "proposed": (args.proposed_ref, args.skip_proposed),
        "testing": (args.testing_ref, args.skip_testing),
}.items():
    if skip:
        continue
    render(name, ref, args)
