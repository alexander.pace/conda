#!/bin/bash
#
# IGWN Conda Distribution tests for LALMetaIO
#

# use GWpy's example XL file
EXAMPLE_XML="$(python -c "import site; print(site.getsitepackages()[0])")/gwpy/testing/data/X1-GWPY_TEST_SEGMENTS-0-10.xml.gz"

_TMPDIR=$(mktemp -d)
trap "popd; rm -rf ${_TMPDIR}" EXIT
pushd ${_TMPDIR}

# reformat ilwd:char columns
cat ${EXAMPLE_XML} | ligolw_no_ilwdchar --verbose > test.xml

# print a table
ligolw_print test.xml -t segment -c start_time -c end_time -d ' ' | tee segments.txt

# delete a table
cat test.xml | ligolw_cut --delete-table segment_definer > test2.xml

# convert from ASCII
ligolw_segments -o segments.xml --insert-from-segwizard=X1=segments.txt
ligolw_print segments.xml -t segment -c start_time -c end_time -d ' '
