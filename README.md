# IGWN Conda Distribution

The IGWN Conda Distribution is a programme to manage and
distribute software and environments used by the International
Gravitational-Wave Observatory Network (IGWN) using the
[conda](https://conda.io) package manager, the
[conda-forge](https://conda-forge.org) community, and the
[CernVM File System (CVMFS)](https://cernvm.cern.ch/portal/filesystem).

The distribution consists of a curated list of packages that are rendered
into highly-specified software environment files that can then be downloaded
and installed on most machines.
For Linux, the environments are created and updated automatically and
distributed globally using CVMFS and
[OASIS](https://computing.docs.ligo.org/guide/cvmfs/#oasis.opensciencegrid.org).

## What is Conda?

[Conda](https://conda.io) is an open source package management system.
It enables users of Windows, macOS, or Linux, to create, save, load, and switch
between software environments on your computer.

## What environments are available?

For full details of what environments are included in the distribution, and
their contents, see
[_Environments_](https://computing.docs.ligo.org/conda/environments/).

## How do I use the IGWN Conda Distribution?

For instructions on how to use Conda and the IGWN Conda Distribution, see
[_Usage_](https://computing.docs.ligo.org/conda/usage/).

!!! tip "More helpful hints"
    See [_Tips and tricks_](https://computing.docs.ligo.org/conda/tips/)
    for more useful hints to help you best utilise the IGWN Conda Distribution.

## Contributing

If you would like to improve the IGWN Conda distributions, please
consider one of the following actions:

  - [Report a problem](https://git.ligo.org/computing/conda/issues/new?issuable_template=Bug)
  - [Request a new/updated package](https://git.ligo.org/sccb/requests/issues/new)
