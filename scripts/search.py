#!/usr/bin/env python

"""Find a package for the relevant platforms
"""

import argparse
import json
import re
import subprocess
from itertools import filterfalse
from shutil import which

from conda_build.metadata import ARCH_MAP

PY_BUILD_REG = re.compile(r"(\A|_)py(\d\d)h")
CONDA = which("conda") or "conda"

SUBDIRS = {
    "linux": [
        "linux-64",
        "linux-aarch64",
        "linux-ppc64le",
    ],
    "osx": [
        "osx-64",
        "osx-arm64",
    ],
    "win": [
        "win-64",
    ],
}


def _is_pypy_build(package):
    """Identify python packages built against pypy instead of cpython

    We probably don't want them.
    """
    for d in package["depends"]:
        if d.startswith("pypy"):
            return True


def conda_search_platforms(
        package,
        version,
        build,
        platforms,
        **kwargs,
):
    for platform in platforms:
        for subdir in SUBDIRS[platform]:
            try:
                pkgs = conda_search(name, version, build, subdir, **kwargs)
            except RuntimeError:
                if not subdir.endswith("-64"):  # skip over non x86_64
                    continue
                raise
            for package in pkgs:
                yield package
                # only search once for noarch
                if package["subdir"] == "noarch":
                    return



def conda_search(package, version, build, subdir, conda="conda", **kwargs):
    key = "=".join((package, version) + ((build,) if build else ()))
    cmd = [
        conda,
        "search",
        key,
        "--subdir={}".format(subdir),
        "--json",
    ]
    out = subprocess.run(
        cmd,
        capture_output=True,
        check=False,
        shell=conda=="conda",
    )
    content = json.loads(out.stdout)
    if out.returncode:
        msg = "{} failed".format(" ".join(cmd))
        try:
            msg += f":\n\n{content['exception_name']}: {content['message']}"
        except KeyError:
            pass
        raise RuntimeError(msg)

    # exclude pypy builds from search hits
    return filterfalse(_is_pypy_build, content[package])


def yaml_build_string(packagedata):
    build = packagedata["build"]
    subdir = packagedata["subdir"]
    selectors = []
    if subdir != "noarch":
        plat, arch = subdir.split("-", 1)
        arch = ARCH_MAP.get(arch, arch)
        selectors.extend((plat, arch))
    if (match := PY_BUILD_REG.search(build)):
        selectors.append("py=={}".format(match.groups()[-1]))
    text = "  build_string: {}".format(build)
    if selectors:
        text += "  # [{}]".format(" and ".join(selectors))
    return text


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "package",
        help="name of package to find",
    )
    parser.add_argument(
        "version",
        help="version of package to find",
    )
    parser.add_argument(
        "-b",
        "--build",
        help="build string of package to find",
    )
    parser.add_argument(
        "-l",
        "--skip-linux",
        action="store_true",
        help="skip linux",
    )
    parser.add_argument(
        "-o",
        "--skip-osx",
        action="store_true",
        help="skip osx",
    )
    parser.add_argument(
        "-w",
        "--skip-windows",
        action="store_true",
        help="skip windows",
    )
    parser.add_argument(
        "-c",
        "--conda",
        default=CONDA,
        help="path of conda executable",
    )
    parser.add_argument(
        "-f",
        "--format",
        choices=["ascii", "yaml"],
        default="yaml",
        help="output format",
    )
    return parser


if __name__ == "__main__":
    parser = create_parser()
    args = parser.parse_args()
    name = args.package
    version = args.version
    build = args.build
    conda = args.conda

    # -- execute searches

    if args.format == "yaml":
        print(
            "package:\n"
            "  name: {}\n"
            "  version: {}".format(name, version)
        )

    # pick platforms
    platforms = [
        platform for skip, platform in [
            (args.skip_linux, "linux"),
            (args.skip_osx, "osx"),
            (args.skip_windows, "win"),
        ] if not skip
    ]

    # search for all packages across all platforms
    for package in conda_search_platforms(
            name,
            version,
            build,
            platforms,
            conda=conda,
    ):
        if args.format == "yaml":
            print(yaml_build_string(package))
        else:
            print(
                "{0[subdir]:9s} {0[name]} {0[version]} {0[build]}".format(
                    package,
                ),
            )
