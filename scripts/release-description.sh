#!/bin/bash
#
# Render the 'description' for a gitlab release for the
# new stable release of the IGWN Conda Distribution
#

set -e

OLD_TAG=$(git tag --list | tail -n2 | head -n1)
NEW_TAG=$(git tag --list | tail -n1)
DATE_STR=$(date -d ${NEW_TAG} +"%d %B %Y")

cat <<EOF
The ${NEW_TAG} tag represents the stable release of the IGWN Conda Distribution
for ${DATE_STR}.

For a full diff between old and new environments please refer to the git
history, or \`conda list\`.

**Summary of changes relative to ${OLD_TAG}**:

[[_TOC_]]

:zap: These changes only refer to the Linux environments, for macOS or Windows
please refer to the git history, or online documentation.

$(python scripts/diff.py ${OLD_TAG} ${NEW_TAG} | tail -n +3)
EOF
