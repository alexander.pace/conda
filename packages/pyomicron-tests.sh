#!/bin/bash
#
# IGWN Conda Distribution tests for PyOmicron
#

# test all of the entry points
omicron-hdf5-merge --help
omicron-process --help
omicron-root-merge --help
omicron-show --help
omicron-status --help
