#!/bin/bash
#
# IGWN Conda Distribution tests for GstLAL
#

export GSTLAL_FIR_WHITEN=0
export TMPDIR=$(python -c "import tempfile; print(tempfile.gettempdir())")

# libs
pkg-config --print-errors --libs gstlal

# python
python -c "
# basic
import gstlal
# compiled modules
import gstlal.misc
"

# gstreamer
gst-inspect-1.0 gstlal

# executables
gstlal_fake_frames --help
