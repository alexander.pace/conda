# Build IGWN Conda distribution using Docker
#
# Copyright (c) 2019 Duncan Macleod <duncan.macleod@ligo.org>
# This file is licensed under the terms of the MIT license,
# see LICENSE for full terms.
#
# This Dockerfile creates a container that should not
# be used for anything, it exists purely to provide a pristine
# build environment in which to build out the IGWN Conda environments.
#
# Conda requires that the environments are created using the production
# runtime path, which is
#
# /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda
#
# so we build them using `docker build` then unpack them using
# `COPY --from=0 ${INSTALL_PATH} /` so as to upload and deploy them
# officially to OASIS.

# -- step 1: build the environments in their production location --------------

FROM debian:buster

ARG INSTALL_PATH=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/
ARG BUILD_OPTS=""

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN apt-get update --quiet --fix-missing && \
    apt-get install --quiet --yes \
      "bash" \
      "curl" \
      "libglib2.0-0"

# install miniconda and configure the base environment
COPY igwn_conda_config.yaml /tmp/
RUN mkdir -p $(dirname ${INSTALL_PATH}) && \
    curl --location \
      https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
      --output /root/miniconda.sh && \
    /bin/bash /root/miniconda.sh -b -p ${INSTALL_PATH} && \
    rm -f ~/miniconda.sh && \
    ${INSTALL_PATH}/condabin/conda config --system --prepend channels conda-forge && \
    ${INSTALL_PATH}/condabin/conda config --system --append channels igwn && \
    ${INSTALL_PATH}/bin/python -c "import ruamel_yaml; yaml = ruamel_yaml.YAML(); config = yaml.load(open('/tmp/igwn_conda_config.yaml', 'r')); print(' '.join(config['base-packages']))" | xargs -t ${INSTALL_PATH}/condabin/conda install --yes && \
    rm -rf ${INSTALL_PATH}/pkgs/* && touch ${INSTALL_PATH}/pkgs/urls.txt

# copy rendered environments
COPY environments/linux /tmp/environments
COPY scripts/*.py /tmp/
RUN ${INSTALL_PATH}/bin/python \
      "/tmp/build.py" \
      "/tmp/environments" \
      --conda "${INSTALL_PATH}/condabin/conda" \
      --envs-dir "${INSTALL_PATH}/envs" \
      --verbose \
      ${BUILD_OPTS} && \
    rm -rf ${INSTALL_PATH}/pkgs/* && touch ${INSTALL_PATH}/pkgs/urls.txt

# -- step 2: unpack them into / to act as a tarball ---------------------------

FROM scratch
ARG INSTALL_PATH=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/
COPY --from=0 ${INSTALL_PATH} /
