#!/bin/bash
#
# IGWN Conda Distribution tests for CWInPy
#

# run everything in a temporary directory
TMPDIR=$(mktemp -d -t cwinpy-XXXXXXXXXX)
trap "cd ~ && rm -rf ${TMPDIR}" EXIT
pushd ${TMPDIR}

# download the original tarball which includes the tests/
VERSION=$(python -c "import cwinpy; print(cwinpy.__version__)")
URL="https://pypi.io/packages/source/c/cwinpy/cwinpy-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/test" || {
	echo "download failed, skipping...";
	exit 77;
}

# run the test suite
python -m pytest -v -ra test/
