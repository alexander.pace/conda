#!/bin/sh

set -ex

export LIGO_DATAFIND_SERVER="datafind.gw-openscience.org:80"

python -m gwdatafind --show-types
python -m gwdatafind -o H -t H1_GWOSC_O2_4KHZ_R1 --latest
